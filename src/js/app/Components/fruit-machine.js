import React, { Component } from 'react';
import Slots from './slots/slots';
import Play from './play';
import Message from './message';

class FruitMachine extends Component {
  constructor() {
    super();
    this.state = {
      slots: Array.from({ length: 3 }, (v, i) => ({ id: `slot${ i }`, color: 'green' })),
    };
    this.handler = this.handler.bind(this);
  }

  handler() {
    this.setState({
      slots: Array.from({ length: 3 }, (v, i) => {
        const colors = ['red', 'blue', 'green', 'yellow'];
        return {
          id: `slot${ i + 1 }`,
          color: colors[Math.floor(Math.random() * 4)],
        };
      }),
    });
  }

  render() {
    return (
      <div className='fruit-machine'>
        <Slots slots={ this.state.slots } />
        <br />
        <Play handler={ this.handler } />
        <br />
        <br />
        <Message colors={ this.state.slots.map(slot => slot.color) } />
      </div>
    );
  }
}

export default FruitMachine;
