import React from 'react';
import { shallow } from 'enzyme';
import Message from './message';

describe('<Message />', () => {
  it('should be a section with a message class', () => {
    const wrapper = shallow(<Message />);
    expect(wrapper.is('section.message')).toBe(true);
  });

  it('should have the text "Message:" in it', () => {
    const wrapper = shallow(<Message />);
    expect(wrapper.text().indexOf('Message:') !== -1).toBe(true);
  });

  it('should start with "Message: Lets Play!"', () => {
    const wrapper = shallow(<Message />);
    expect(wrapper.find('section').text()).toBe('Message: Lets Play!');
  });

  it('should say "Message: Unlucky Try Again!" when colors dont match', () => {
    const wrapper = shallow(<Message />);
    wrapper.setProps({ colors: ['red', 'green', 'blue'] });
    expect(wrapper.find('section').text()).toBe('Message: Unlucky Try Again!');
  });

  it('should say "Message: WINNER!!!" when colors match', () => {
    const wrapper = shallow(<Message />);
    wrapper.setProps({ colors: ['blue', 'blue', 'blue'] });
    expect(wrapper.find('section').text()).toBe('Message: WINNER!!!');
  });
});
