import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Play extends Component {
  render() {
    return (
      <button className={ 'play' } onClick={ this.props.handler }>
        PLAY
      </button>
    );
  }
}

Play.propTypes = {
  handler: PropTypes.func.isRequired,
};

Play.defaultProps = {
  handler: () => {},
};

export default Play;
