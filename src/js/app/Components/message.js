import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Message extends Component {
  constructor() {
    super();
    this.isGameStarted = false;
  }

  updateMessage(colors) {
    const isWinner = colors.every((color, i) => {
      if (i === 0) {
        return true;
      }
      return color === colors[i - 1];
    }) && this.isGameStarted;

    if (!this.isGameStarted) {
      this.isGameStarted = true;
      return 'Lets Play!';
    } else if (!isWinner) {
      return 'Unlucky Try Again!';
    }

    return 'WINNER!!!';
  }

  render() {
    return (
      <section className={ 'message' }>
        Message: { this.updateMessage(this.props.colors) }
      </section>
    );
  }
}

Message.propTypes = {
  colors: PropTypes.array.isRequired,
};

Message.defaultProps = {
  colors: [],
};

export default Message;
