import React from 'react';
import { shallow } from 'enzyme';
import FruitMachine from './fruit-machine';

describe('<FruitMachine />', () => {
  it('should be a div with a fruit-machine class', () => {
    const wrapper = shallow(<FruitMachine />);
    expect(wrapper.is('div.fruit-machine')).toBe(true);
  });

  it('should have an Slots Component', () => {
    const wrapper = shallow(<FruitMachine />);
    expect(wrapper.find('Slots').exists()).toBe(true);
  });

  it('should have an Play Component', () => {
    const wrapper = shallow(<FruitMachine />);
    expect(wrapper.find('Play').exists()).toBe(true);
  });

  it('should have an Message Component', () => {
    const wrapper = shallow(<FruitMachine />);
    expect(wrapper.find('Message').exists()).toBe(true);
  });

  it('should update state appropriately when handler is run', () => {
    const wrapper = shallow(<FruitMachine />);
    // set state to a colour that won't exist in the slots to prove point
    wrapper.setState({
      slots: Array.from({ length: 3 }, (v, i) => ({ id: `slot${ i }`, color: 'purple' })),
    });
    wrapper.props().children.forEach(child => {
      if (child.type.name === 'Play') child.props.handler();
    });
    expect(wrapper.state('slots').every(slot => {
      return ['red', 'blue', 'green', 'yellow'].indexOf(slot.color) !== -1;
    })).toBe(true);
  });
});
