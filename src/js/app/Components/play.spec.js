import React from 'react';
import { shallow } from 'enzyme';
import Play from './play';

describe('<Play />', () => {
  it('should be a button with a play class', () => {
    const wrapper = shallow(<Play />);
    expect(wrapper.is('button.play')).toBe(true);
  });

  it('should have text PLAY', () => {
    const wrapper = shallow(<Play />);
    expect(wrapper.text()).toBe('PLAY');
  });

  it('should have run the given function when clicked', () => {
    const foo = {
      bar: () => {},
    };
    const barSpy = spyOn(foo, 'bar');
    const wrapper = shallow(<Play handler={ foo.bar } />);
    wrapper.find('button').simulate('click');
    expect(barSpy).toHaveBeenCalled();
  });
});
