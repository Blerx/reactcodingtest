import React from 'react';
import { shallow } from 'enzyme';
import SlotItem from './slot-item';

describe('<SlotItem />', () => {
  it('should be a li with a slot class', () => {
    const wrapper = shallow(<SlotItem />);
    expect(wrapper.is('li.slot')).toBe(true);
  });

  it('should initially have text green', () => {
    const wrapper = shallow(<SlotItem />);
    expect(wrapper.text()).toBe('green');
  });

  it('should have props with color property', () => {
    const wrapper = shallow(<SlotItem />);
    const inst = wrapper.instance();
    expect(inst.props.color).not.toBe(undefined);
  });
});
