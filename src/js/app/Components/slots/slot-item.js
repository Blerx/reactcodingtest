import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SlotItem extends Component {
  render() {
    return (
      <li className='slot'>{ this.props.color }</li>
    );
  }
}

SlotItem.defaultProps = {
  color: 'green',
};

SlotItem.PropTypes = {
  color: PropTypes.string,
};

export default SlotItem;
