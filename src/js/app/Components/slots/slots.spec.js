import React from 'react';
// import ReactShallowRenderer from 'react-test-renderer/shallow';
import { shallow } from 'enzyme';
import Slots from './slots';

describe('<Slots />', () => {
  beforeEach(function () {
    this.items = Array.from({ length: 3 }, (v, i) => ({ id: `slot${ i }`, value: 'green' }));
  });

  it('should be a ul with a slots class', () => {
    const wrapper = shallow(<Slots />);
    expect(wrapper.is('ul.slots')).toBe(true);
  });

  it('should contain 3 SlotItem components', function () {
    const wrapper = shallow(<Slots slots={ this.items } />);
    expect(wrapper.find('SlotItem').length).toBe(3);
  });
});
