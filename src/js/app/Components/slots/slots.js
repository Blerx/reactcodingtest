import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SlotItem from './slot-item';

class Slots extends Component {
  render() {
    let slotItems;
    if (this.props.slots && this.props.slots.length) {
      slotItems = this.props.slots.map(slot => {
        return (
          <SlotItem key={ slot.id } color={ slot.color } />
        );
      });
    }
    return (
      <ul className='slots'>
        {slotItems}
      </ul>
    );
  }
}

Slots.propTypes = {
  slots: PropTypes.array,
};

Slots.defaultProps = {
  slots: [],
};

export default Slots;
