import React, { Component } from 'react';
import FruitMachine from './Components/fruit-machine';

class App extends Component {
  render() {
    return (
      <div className='hero'>
        <h1>FRUIT MACHINE</h1>
        <br />
        <FruitMachine />
      </div>
    );
  }
}

export default App;
