import React from 'react';
import { shallow } from 'enzyme';
import App from './index';

describe('<App/>', () => {
  it('should be a div with a hero class', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.is('div.hero')).toBe(true);
  });

  it('should have a h1 tag', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find('h1').exists()).toBe(true);
  });

  it('should have a h1 with text FRUIT MACHINE', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find('h1').text()).toBe('FRUIT MACHINE');
  });

  it('should contain a FruitMachine element', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find('FruitMachine').exists()).toBe(true);
  });
});
